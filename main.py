# %matplotlib inline
# %load_ext autoreload
# %reload_ext autoreload

# Import Keras and other Deep Learning dependencies
from keras.models import Sequential
import time
from keras.optimizers import Adam
from keras.layers import Conv2D, ZeroPadding2D, Activation, Input, concatenate
from keras.models import Model
from keras.layers.pooling import MaxPooling2D, AveragePooling2D
from keras.layers.core import Lambda, Flatten, Dense
from sklearn.preprocessing import LabelBinarizer
from keras.optimizers import *
from keras import backend as K
from keras.regularizers import l2
K.set_image_data_format('channels_last')
from load_data import *
from Siamese_Loader import Siamese_Loader
import matplotlib.pyplot as plt

data_path = os.path.join('./omniglot')
train_folder = os.path.join(data_path,'images_background')
valpath = os.path.join(data_path,'images_evaluation')
base_class_name = 'character'

with open(os.path.join(data_path, "train.pickle"), "rb") as f:
    (X, classes) = pickle.load(f)

with open(os.path.join(data_path, "val.pickle"), "rb") as f:
    (Xval, val_classes) = pickle.load(f)

def gen_class_names(base_class_name):
    classes = []
    for i in range(1,21):
        if i < 10:
            classes.append("{0}0{1}".format(base_class_name, i))
        else:
            classes.append("{0}{1}".format(base_class_name, i))
    return classes

def generate_one_hot_encoding(classes):
    encoder = LabelBinarizer()
    transfomed_labels = encoder.fit_transform(classes)
    return transfomed_labels

def initialize_weights(shape, name=None):
    """
        The paper, http://www.cs.utoronto.ca/~gkoch/files/msc-thesis.pdf
        suggests to initialize CNN layer weights with mean as 0.0 and standard deviation of 0.01
    """
    return np.random.normal(loc = 0.0, scale = 1e-2, size = shape)

def initialize_bias(shape, name=None):
    """
        The paper, http://www.cs.utoronto.ca/~gkoch/files/msc-thesis.pdf
        suggests to initialize CNN layer bias with mean as 0.5 and standard deviation of 0.01
    """
    return np.random.normal(loc = 0.5, scale = 1e-2, size = shape)

def get_siamese_model(input_shape):
    """
        Model architecture based on the one provided in: http://www.cs.utoronto.ca/~gkoch/files/msc-thesis.pdf
    """
    left_input = Input(input_shape)
    right_input = Input(input_shape)
    model = Sequential()
    model.add(Conv2D(64, (10,10), activation='relu', input_shape=input_shape,
                   kernel_initializer=initialize_weights, kernel_regularizer=l2(2e-4)))
    model.add(MaxPooling2D())
    model.add(Conv2D(128, (7,7), activation='relu',
                     kernel_initializer=initialize_weights,
                     bias_initializer=initialize_bias, kernel_regularizer=l2(2e-4)))
    model.add(MaxPooling2D())
    model.add(Conv2D(128, (4,4), activation='relu', kernel_initializer=initialize_weights,
                     bias_initializer=initialize_bias, kernel_regularizer=l2(2e-4)))
    model.add(MaxPooling2D())
    model.add(Conv2D(256, (4,4), activation='relu', kernel_initializer=initialize_weights,
                     bias_initializer=initialize_bias, kernel_regularizer=l2(2e-4)))
    model.add(Flatten())
    model.add(Dense(4096, activation='sigmoid',
                   kernel_regularizer=l2(1e-3),
                   kernel_initializer=initialize_weights,bias_initializer=initialize_bias))
    encoded_l = model(left_input)
    encoded_r = model(right_input)
    L1_layer = Lambda(lambda tensors:K.abs(tensors[0] - tensors[1]))
    L1_distance = L1_layer([encoded_l, encoded_r])
    prediction = Dense(1,activation='sigmoid',bias_initializer=initialize_bias)(L1_distance)
    siamese_net = Model(inputs=[left_input,right_input],outputs=prediction)
    return siamese_net

def train():
    evaluate_every = 10  # interval for evaluating on one-shot tasks
    loss_every = 20  # interval for printing loss (iterations)
    batch_size = 32
    n_iter = 20000
    N_way = 20  # how many classes for testing one-shot tasks>
    n_val = 250  # how many one-shot tasks to validate on?
    best = -1
    weights_path_2 = os.path.join(data_path, "model_weights.h5")
    print("Starting training process!")
    print("-------------------------------------")
    t_start = time.time()
    for i in range(1, n_iter):
        (inputs, targets) = loader.get_batch(batch_size)
        loss = model.train_on_batch(inputs, targets)
        print("\n ------------- \n")
        print("Loss: {0}".format(loss))
        if i % evaluate_every == 0:
            print("Time for {0} iterations: {1}".format(i, time.time() - t_start))
            val_acc = loader.test_oneshot(model, N_way, n_val, verbose=True)
            if val_acc >= best:
                print("Current best: {0}, previous best: {1}".format(val_acc, best))
                print("Saving weights to: {0} \n".format(weights_path_2))
                model.save_weights(weights_path_2)
                best = val_acc

        if i % loss_every == 0:
            print("iteration {}, training loss: {:.2f},".format(i, loss))

    weights_path_2 = os.path.join(data_path, "model_weights.h5")
    model.load_weights(weights_path_2)

def test():
    def nearest_neighbour_correct(pairs, targets):
        """returns 1 if nearest neighbour gets the correct answer for a one-shot task
            given by (pairs, targets)"""
        L2_distances = np.zeros_like(targets)
        for i in range(len(targets)):
            L2_distances[i] = np.sum(np.sqrt(pairs[0][i] ** 2 - pairs[1][i] ** 2))
        if np.argmin(L2_distances) == np.argmax(targets):
            return 1
        return 0

    def test_nn_accuracy(N_ways, n_trials, loader):
        """Returns accuracy of one shot """
        print("Evaluating nearest neighbour on {} unique {} way one-shot learning tasks ...".format(n_trials, N_ways))

        n_right = 0

        for i in range(n_trials):
            pairs, targets = loader.make_oneshot_task(N_ways, "val")
            correct = nearest_neighbour_correct(pairs, targets)
            n_right += correct
        return 100.0 * n_right / n_trials

    ways = np.arange(1, 30, 2)
    resume = False
    val_accs, train_accs, nn_accs = [], [], []
    trials = 450
    for N in ways:
        val_accs.append(loader.test_oneshot(model, N, trials, "val", verbose=True))
        train_accs.append(loader.test_oneshot(model, N, trials, "train", verbose=True))
        nn_accs.append(test_nn_accuracy(N, trials, loader))

if __name__ == '__main__':
    classes = gen_class_names(base_class_name)
    labels = generate_one_hot_encoding(classes)

    model = get_siamese_model((105, 105, 1))
    model.summary()
    optimizer = Adam(lr=0.00006)
    model.compile(loss="binary_crossentropy", optimizer=optimizer)

    loader = Siamese_Loader(data_path)

    train()
